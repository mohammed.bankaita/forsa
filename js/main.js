var editButton = document.getElementById("edit_profile_btn")
var cancleButton= document.getElementById("cancel-btn")

editButton.addEventListener("click", function() {
    document.getElementById("user_profile").classList.add("hideProfile");
    document.getElementById("user_profile").classList.remove("displayProfile");
    document.getElementById("edit-user-profile").classList.add("displayProfile");
    document.getElementById("edit-user-profile").classList.remove("hideProfile")
})
cancleButton.addEventListener("click", function() {
    document.getElementById("user_profile").classList.add("displayProfile");
    document.getElementById("user_profile").classList.remove("hideProfile");
    document.getElementById("edit-user-profile").classList.add("hideProfile");
    document.getElementById("edit-user-profile").classList.remove("displayProfile")
})